#ifndef GAME_H
#define GAME_H
#include "headers/bad_card.h"
#include "headers/card.h"
#include "headers/login.h"
#include "headers/lucky_card.h"
#include "headers/player.h"
#include "headers/question.h"

#include <QApplication>
#include <QVector>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>

class Game
{

public:
	Game();
	Game(QVector<QString> players);

	~Game();

	void add_questions();
	QList<int> get_all_positions(); // List of indices of players on the board

	Player *get_current_player();
	QList<Player *> get_player_list(); // list of all players
	int get_player_position(Player *player);

	std::string get_current_player_name();	// return current player name
	int get_current_player_index();			// Returns player index in player list
	QList<Question *> get_questions_list(); // Returns the list of questions

	// Game Functions
	void move_player(int true_number_of_questions); // Moves player
	void increment_current_player();				// Changes who's turn it is
	void endturn();									// ends turn

private:
	Player *current_player;
	QList<Player *> player_list;
	QList<Question *> questions_list;
};

#endif // GAME_H

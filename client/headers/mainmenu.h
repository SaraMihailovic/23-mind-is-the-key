#ifndef MAINMENU_H
#define MAINMENU_H

#include <QDialog>

namespace Ui
{
class MainMenu;
}

class MainMenu : public QDialog
{
	Q_OBJECT

public:
	explicit MainMenu(QWidget *parent = nullptr);
	~MainMenu();

private slots:
	void on_join_clicked();

	void on_local_clicked();

	void on_quit_clicked();

private:
	Ui::MainMenu *ui;
	void set_background_image(QString image_path);
};

#endif // MAINMENU_H

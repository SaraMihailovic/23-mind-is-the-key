#ifndef CARDARRAY_H
#define CARDARRAY_H

#include "headers/bad_card.h"
#include "headers/card.h"
#include "headers/lucky_card.h"
#include "headers/player.h"
#include "mainwindow.h"

class CardArray
{

public:
	CardArray();
	~CardArray();

	void create_cards(Player *currentPlayer, Ui::MainWindow *ui);

private:
	std::vector<LuckyType> v2;
	std::vector<BadType> v;
};
#endif

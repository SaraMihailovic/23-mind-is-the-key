# Mind-is-the-key  :thought_balloon: :key:

![izgled igre](screenshots/week7.png)


# Opis igre :memo: :

Mind is the key je 2D igrica koja predstavlja takmičenje u znanju između više takmičara (broj mogućih igrača je od 2 do 4). Igra se može igrati na dva načina preko mreže ili lokalno. Igrači odgovaraju naizmenično na pitanja, i pomeraju se po tabli za onoliko koraka koliko daju tačnih odgovora. Nakon svakog pomeranja po tabli igrač može ako želi okušati sreću i kliknuti na karticu koja može biti dobra ili loša i dobiti nagradu ili kaznu. Cilj je stići što dalje na tabli, a pobednik je onaj koji najdalje stigne kada sva pitanja iz liste nestanu i osvaja sve što je na tom putu sakupio - novac, razna putovanja, slatkiše. Iskušajte svoju spretnost, brzinu i sreću, a pre svega testirajte svoje znanje.



# Korisćene biblioteke :books: :
* Qt >= 5.12 

# Instalacija potebnih biblioteka :hammer: :
## Qt i Qt Creator
1. `$ sudo apt install qtcreator`
2. `$ sudo apt install build-essential`
3. `$ sudo apt install qt5-default`
4. `$ sudo apt install qtmultimedia5-dev`

# Instalacija i pokretanje :wrench: :
1. Klonirajte repozitorijum: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/23-mind-is-the-key`
2. Pozicionirajte se u direktorijum projekta: `$ cd 23-mind-is-the-key`
3. Kreirajte build direktorijum: `$ mkdir build` <br>
   Pozicionirajte se u build direktorijum: `$ cd build` <br>
   `$ cmake  -G "Unix MakeFiles" ..` <br>
   `$ make` <br>
4. Pokretanje servera: `$ ./Server` <br>
   Pokretanje glavnog prozora: `$ ./MindIsTheKey`

# Upustvo za igranje :video_game: :
## :one: Lokalno
1. Pokrenuti `./MindIsTheKey`
2. Izabrati opicju **Local** 
3. Odabrati broj igrača i uneti imena

## :two: Preko servera
1. Pokrenuti `./Server` u pozadini da radi
2. Pokrenite klijenata(2-4) koliko želite sa `./MindIsTheKey`
3. Na svakom klijentu izabrati opciju **Join**, uneti ime, i kliknuti na **connect**

# Demo snimak igrice :movie_camera: :

Demo snimak se nalazi na početku stranice pod nazivom presentation.mp4

# Dodatni resursi  :musical_score: :

- slike su delo autora projekta
- zvuk u pozadini je pesma sa sajta  [Bensound.com](https://www.bensound.com/)



## Developers :

- [Tamara Sljivic, 107/2017](https://gitlab.com/TamaraSljivic)  :woman:
- [Tamara Stojkovic, 178/2017](https://gitlab.com/tamarastojkovic) :woman:
- [Sara Mihailovic, 293/2017](https://gitlab.com/SaraMihailovic) :woman:
- [Jovana Markovic, 144/2017](https://gitlab.com/jovanaMarkovic) :woman:

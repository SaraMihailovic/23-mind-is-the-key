#ifndef SERVERGAME_H
#define SERVERGAME_H

#include "headers/game.h"
#include "headers/player.h"
#include "headers/question.h"
#include "headers/questionsbox.h"
#include "ui_mainwindow.h"

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QTcpSocket>
#include <QtCore>
#include <QtGui>

namespace Ui
{
class MainWindow;
}

class ServerGame : public QMainWindow
{
	Q_OBJECT
public:
	explicit ServerGame(QTcpSocket *socket, QVector<QString> players, QWidget *parent = nullptr);
	~ServerGame();

	void update_display(); // update graphics on board
	void set_space_list(); // generate space list from elements

public slots:
	void socketReadyRead();
	void socketDisconnected();

	void on_roll_button_clicked();
	void on_card_button_clicked();
	void on_end_roll_button_clicked();
	void on_question_button_clicked();

private:
	QTcpSocket *socket;
	QDataStream in;

	Ui::MainWindow *ui;
	Game *game;
	int die;
	int true_answers;

	QList<Question *> questions_list;
	QList<QLabel *> space_list;	 // list of label elements in the ui
	QList<Player *> player_list; // list of player

	QVector<QLabel *> money_labels;
	QVector<QLabel *> sweets_labels;
	QVector<QLabel *> travel_labels;
	QVector<QLabel *> immunity_labels;
};

#endif // SERVERGAME_H

#include "headers/login.h"

#include "headers/mainwindow.h"
#include "ui_login.h"

#include <QMediaPlayer>
#include <QMessageBox>

Login::Login(QWidget *parent) : QDialog(parent), ui(new Ui::Login)
{
	ui->setupUi(this);
	set_background_image(":images/images/paper1.png");

	QFont f("Times New Roman", 20);				// Creates new font to use
	ui->label_2->setFont(f);					// Sets font
	ui->label_2->setAlignment(Qt::AlignCenter); // Aligns text to center
	this->setWindowTitle("Login");				// Sets window title

	// Allowed only number
	ui->lineEdit->setValidator(new QIntValidator(2, 4, this));

	ui->button->setEnabled(false);
	ui->buttonBox->setEnabled(false);
	ui->lineEdit1->setVisible(false);
	ui->lineEdit2->setVisible(false);
	ui->lineEdit3->setVisible(false);
	ui->lineEdit4->setVisible(false);

	ui->p1_name->setVisible(false);
	ui->p2_name->setVisible(false);
	ui->p3_name->setVisible(false);
	ui->p4_name->setVisible(false);

	ui->mute->setStyleSheet("border-image:url(:images/images/mute.png);");
	ui->unmute->setStyleSheet("border-image:url(:images/images/unmute.png);");

	ui->mute->setVisible(false);
	ui->unmute->setVisible(true);

	musica = new QMediaPlayer();
	musica->setMedia(QUrl("qrc:/sounds/sounds/song.mp3"));
	musica->play();
}

Login::~Login()
{
	delete ui;
}

QString Login::get_player_one()
{
	return player_one;
}

QString Login::get_player_two()
{
	return player_two;
}

QString Login::get_player_three()
{
	return player_three;
}

QString Login::get_player_four()
{
	return player_four;
}

int Login::get_players()
{
	return players;
}

void Login::on_button_clicked()
{
	int ind = 0;
	ui->label_notes->setText("");

	if (ind_for_number == true && players == 4) {
		player_one = ui->lineEdit1->text(); // Store usernames
		player_two = ui->lineEdit2->text();
		player_three = ui->lineEdit3->text();
		player_four = ui->lineEdit4->text();

		p1 = player_one.toUtf8().constData();
		p2 = player_two.toUtf8().constData();
		p3 = player_three.toUtf8().constData();
		p4 = player_four.toUtf8().constData();

		if (p1 == "" or p2 == "" or p3 == "" or p4 == "") {
			ind = 1;
			ui->label_notes->setText("You have to enter names for all players!");
		}
	} else if (ind_for_number == true && players == 3) {

		player_one = ui->lineEdit1->text();
		player_two = ui->lineEdit2->text();
		player_three = ui->lineEdit3->text();

		p1 = player_one.toUtf8().constData();
		p2 = player_two.toUtf8().constData();
		p3 = player_three.toUtf8().constData();

		if (p1 == "" or p2 == "" or p3 == "") {
			ind = 1;
			ui->label_notes->setText("You have to enter names for all players!");
		}

	} else if (ind_for_number == true && players == 2) {
		player_one = ui->lineEdit1->text();
		player_two = ui->lineEdit2->text();

		p1 = player_one.toUtf8().constData();
		p2 = player_two.toUtf8().constData();

		if (p1 == "" or p2 == "") {
			ind = 1;
			ui->label_notes->setText("You have to enter names for all players!");
		}
	}

	if (ind == 1) {
		ui->buttonBox->setEnabled(false);
	} else {
		ui->buttonBox->setEnabled(true); // Log in with "ok"
	}
}

void Login::on_confirm_button_clicked()
{
	int ind = 0;
	if (ui->lineEdit->text() == "4") {
		players = 4;
		ui->lineEdit1->setVisible(true);
		ui->lineEdit2->setVisible(true);
		ui->lineEdit3->setVisible(true);
		ui->lineEdit4->setVisible(true);

		ui->p1_name->setVisible(true);
		ui->p2_name->setVisible(true);
		ui->p3_name->setVisible(true);
		ui->p4_name->setVisible(true);

		ui->label_notes->setText("You confirmed 4 players for this game!");

	}

	else if (ui->lineEdit->text() == "3") {
		players = 3;
		ui->lineEdit1->setVisible(true);
		ui->lineEdit2->setVisible(true);
		ui->lineEdit3->setVisible(true);

		ui->p1_name->setVisible(true);
		ui->p2_name->setVisible(true);
		ui->p3_name->setVisible(true);

		ui->label_notes->setText("You confirmed 3 players for this game!");
	}

	else if (ui->lineEdit->text() == "2") {
		players = 2;
		ui->lineEdit1->setVisible(true);
		ui->lineEdit2->setVisible(true);

		ui->p1_name->setVisible(true);
		ui->p2_name->setVisible(true);

		ui->label_notes->setText("You confirmed 2 players for this game!");

	}

	else if (ui->lineEdit->text().toInt() > 4) {
		ui->label_notes->setText("You must have at most 4 players!");
		ind = 1;
	} else {
		ui->label_notes->setText("You must have at least 2 players!");
		ind = 1;
	}

	ui->confirm_button->setEnabled(false);

	if (ui->lineEdit->text().isEmpty()) {
		ind = 1;
	}
	if (ind == 1) {
		ui->confirm_button->setEnabled(true);
		ui->button->setEnabled(false);
	} else {
		ui->button->setEnabled(true);
	}
	ind_for_number = true;
}

void Login::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
void Login::on_mute_clicked()
{
	musica->play();
	ui->mute->setVisible(false);
	ui->unmute->setVisible(true);
}
void Login::on_unmute_clicked()
{
	musica->stop();
	ui->mute->setVisible(true);
	ui->unmute->setVisible(false);
}

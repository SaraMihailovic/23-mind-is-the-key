#include "headers/mainwindow.h"

#include "headers/bad_card.h"
#include "headers/card.h"
#include "headers/cardarray.h"
#include "headers/die.h"
#include "headers/game.h"
#include "headers/gameover.h"
#include "headers/help.h"
#include "headers/howtoplay.h"
#include "headers/login.h"
#include "headers/lucky_card.h"
#include "headers/question.h"
#include "headers/questionsbox.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QGraphicsScene>
#include <QInputDialog>
#include <QMessageBox>
#include <QtCore>
#include <QtGui>
#include <cstdlib>
#include <ctime>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	Login login;
	login.setModal(true);
	login.exec();

	int players = login.get_players();

	this->setWindowTitle("Mind is the key");

	this->setFixedSize(1250, 604);

	if (players == 2) {
		ui->pic4->setVisible(false);
		ui->p4->setVisible(false);
		ui->pic3->setVisible(false);
		ui->p3->setVisible(false);
		ui->player_name3->setVisible(false);
		ui->player_name4->setVisible(false);

		QString p1_name = login.get_player_one();
		QString p2_name = login.get_player_two();

		player_name.push_back(p1_name);
		player_name.push_back(p2_name);

		ui->player_name1->setText(p1_name);
		ui->player_name2->setText(p2_name);

	} else if (players == 3) {
		ui->pic4->setVisible(false);
		ui->p4->setVisible(false);
		ui->player_name4->setVisible(false);

		QString p1_name = login.get_player_one();
		QString p2_name = login.get_player_two();
		QString p3_name = login.get_player_three();

		player_name.push_back(p1_name);
		player_name.push_back(p2_name);
		player_name.push_back(p3_name);

		ui->player_name1->setText(p1_name);
		ui->player_name2->setText(p2_name);
		ui->player_name3->setText(p3_name);

	} else {

		QString p1_name = login.get_player_one();
		QString p2_name = login.get_player_two();
		QString p3_name = login.get_player_three();
		QString p4_name = login.get_player_four();

		player_name.push_back(p1_name);
		player_name.push_back(p2_name);
		player_name.push_back(p3_name);
		player_name.push_back(p4_name);

		ui->player_name1->setText(p1_name);
		ui->player_name2->setText(p2_name);
		ui->player_name3->setText(p3_name);
		ui->player_name4->setText(p4_name);
	}

	ui->card_button->setEnabled(false);
	ui->roll_button->setEnabled(false);
	ui->end_roll_button->setEnabled(false);

	this->start_game(player_name);
	this->update_display();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::start_game(QVector<QString> player_name)
{

	Game *game = new Game(player_name);
	set_game(game);

	set_space_list();
	this->questions_list = game->get_questions_list();
	this->player_list = game->get_player_list();
}

void MainWindow::update_display()
{

	// reset the board (clears every space)
	for (int i = 0; i < space_list.length(); i++) {
		space_list[i]->setStyleSheet("");
		space_list[i]->clear();
	}

	// update board
	Player *current_player = game->get_current_player();

	for (int j = 0; j < player_list.length(); j++) {
		int index = game->get_player_position(player_list[j]);
		// highlight current player position
		if (player_list[j] == current_player) {
			space_list[index]->setStyleSheet("background: rgba(255, 212, 128, .4)");
		}

		switch (player_list[j]->get_piece()) {
		case 1:
			space_list[index]->setPixmap(QPixmap(":images/images/PhilipMartin.png"));
			break;
		case 2:
			space_list[index]->setPixmap(QPixmap(":images/images/Columbus.png"));
			break;
		case 3:
			space_list[index]->setPixmap(QPixmap(":images/images/Explorer.png"));
			break;
		case 4:
			space_list[index]->setPixmap(QPixmap(":images/images/Magellan.png"));
			break;
		}
	}

	// update current player
	ui->lbl_player->setText(QString::fromStdString(current_player->get_player_name()) + "'s turn");
}

void MainWindow::on_roll_button_clicked()
{
	int number = true_answers;

	game->move_player(number);
	update_display();

	ui->roll_button->setEnabled(false);
	ui->end_roll_button->setEnabled(true);
	ui->card_button->setEnabled(true);
}

void MainWindow::on_end_roll_button_clicked()
{
	game->endturn();
	update_display();

	ui->end_roll_button->setDisabled(true);
	ui->label_card->setText(QString::fromStdString(""));
	ui->card_button->setStyleSheet("border-image:url(:images/images/card.png);");
	ui->card_button->setEnabled(false);
	ui->roll_button->setEnabled(false);
}

void MainWindow::on_question_button_clicked()
{
	Die d;
	die = d.roll_die();

	QList<Question *> questions;
	for (int i = 0; i < die; i++) {
		questions.push_back(this->questions_list[i]);
	}
	if (ind == 0) {
		QuestionsBox questionsbox(questions);
		questionsbox.setModal(true);
		questionsbox.exec();

		true_answers = questionsbox.get_true_answers();

		switch (true_answers) {
		case 0: {
			ui->label->setPixmap(QPixmap(":images/images/die0.png"));
		} break;
		case 1: {
			ui->label->setPixmap(QPixmap(":images/images/die1.png"));
		} break;
		case 2: {
			ui->label->setPixmap(QPixmap(":images/images/die2.png"));
		} break;
		case 3: {
			ui->label->setPixmap(QPixmap(":images/images/die3.png"));
		} break;
		case 4: {
			ui->label->setPixmap(QPixmap(":images/images/die4.png"));
		} break;
		case 5: {
			ui->label->setPixmap(QPixmap(":images/images/die5.png"));
		} break;
		case 6: {
			ui->label->setPixmap(QPixmap(":images/images/die6.png"));
		} break;
		}

		ui->roll_button->setEnabled(true);
		delete_die_questions();
	} else {
		Gameover *gameover = new Gameover(player_list);
		gameover->show();
		this->hide();
	}
}

void MainWindow::delete_die_questions()
{

	for (int i = 0; i < die; i++) {
		this->questions_list.pop_front();
		if (questions_list.size() <= 6) {
			ind = 1;
		}
	}
}

bool label_less_than(QLabel *L1, QLabel *L2)
{
	// sort name label in set_space_list()
	return L1->objectName() < L2->objectName();
}

void MainWindow::set_space_list()
{
	//- - -
	//-   -
	//- - 0 (0-start)
	QList<QLabel *> list = ui->Board->findChildren<QLabel *>();
	std::sort(list.begin(), list.end(), label_less_than);
	list.takeFirst();
	this->space_list = list;
}

void MainWindow::on_card_button_clicked()
{
	Player *currentPlayer = game->get_current_player();

	CardArray *cards = new CardArray();
	cards->create_cards(currentPlayer, this->ui);

	update_display();
}

void MainWindow::on_action_how_to_play_triggered()
{
	HowToPlay how;
	how.setModal(true);
	how.exec();
}
void MainWindow::on_action_help_triggered()
{
	Help hl;
	hl.setModal(true);
	hl.exec();
}
void MainWindow::on_action_quit_triggered()
{
	close();
}

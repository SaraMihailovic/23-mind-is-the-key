#include "headers/question.h"

#include <string>

Question::Question()
{
}

Question::~Question()
{
	m_question = "";
	m_answer = "";
}

void Question::set_question(std::string question)
{
	m_question = question;
}

void Question::set_answer(std::string ans)
{

	m_answer = ans;
}

std::string Question::get_question()
{
	return m_question;
}

std::string Question::get_answer()
{
	return m_answer;
}

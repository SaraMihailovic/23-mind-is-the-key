#include "headers/game.h"

#include "headers/player.h"
#include "headers/question.h"
#include "headers/questionslist.h"

#include <QDebug>
#include <cstdlib>
#include <string>
#include <vector>

Game::Game()
{
	add_questions();

	for (int i = 0; i < 4; i++) {
		Player *player = new Player;   // Creates new player
		player_list.push_back(player); // adds new player to playerList
	}

	current_player = player_list.at(0); // sets current player
}

Game::Game(QVector<QString> player_name)
{
	add_questions();

	std::string PIECE1 = "PhilipMartin";
	std::string PIECE2 = "Columbus";
	std::string PIECE3 = "Explorer";
	std::string PIECE4 = "Magellan";

	if (player_name.size() == 4) {

		std::string PLAYER1 = player_name[0].toStdString();
		std::string PLAYER2 = player_name[1].toStdString();
		std::string PLAYER3 = player_name[2].toStdString();
		std::string PLAYER4 = player_name[3].toStdString();

		Player *player_one = new Player(PLAYER1, PIECE1, 1, 0);
		Player *player_two = new Player(PLAYER2, PIECE2, 2, 0);
		Player *player_three = new Player(PLAYER3, PIECE3, 3, 0);
		Player *player_four = new Player(PLAYER4, PIECE4, 4, 0);

		player_list.push_back(player_one);
		player_list.push_back(player_two);
		player_list.push_back(player_three);
		player_list.push_back(player_four);

	} else if (player_name.size() == 3) {

		std::string PLAYER1 = player_name[0].toStdString();
		std::string PLAYER2 = player_name[1].toStdString();
		std::string PLAYER3 = player_name[2].toStdString();

		Player *player_one = new Player(PLAYER1, PIECE1, 1, 0);
		Player *player_two = new Player(PLAYER2, PIECE2, 2, 0);
		Player *player_three = new Player(PLAYER3, PIECE3, 3, 0);

		player_list.push_back(player_one);
		player_list.push_back(player_two);
		player_list.push_back(player_three);

	} else {

		std::string PLAYER1 = player_name[0].toStdString();
		std::string PLAYER2 = player_name[1].toStdString();

		Player *player_one = new Player(PLAYER1, PIECE1, 1, 0);
		Player *player_two = new Player(PLAYER2, PIECE2, 2, 0);

		player_list.push_back(player_one);
		player_list.push_back(player_two);
	}

	current_player = player_list.at(0);
}

Game::~Game()
{
}

// Gets positions of all players in the board
QList<int> Game::get_all_positions()
{
	QList<int> list;							 // creates list of positions to return
	QList<Player *> players = get_player_list(); // list players
	for (int i = 0; i < players.length(); i++) {
		Player *player = players.at(i); // gets player position in list
		const unsigned int pos = player->get_position();
		list.push_back(pos); // adds to list that will be returned
	}
	return list;
}

Player *Game::get_current_player()
{
	return current_player;
}

QList<Player *> Game::get_player_list()
{
	return player_list;
}

int Game::get_player_position(Player *player)
{
	return player->get_position();
}

int Game::get_current_player_index()
{
	int curr_pos = 0;
	for (int i = 0; i < player_list.length(); i++) {
		if (current_player->get_player_name().compare(player_list.at(i)->get_player_name()) == 0) {
			curr_pos = i;
		}
	}
	return curr_pos;
}

void Game::increment_current_player()
{
	int pos = get_current_player_index();
	if (pos + 1 >= player_list.length()) {
		pos = (pos + 1) % player_list.length();
		current_player = player_list.at(pos);
	} else {
		current_player = player_list.at(pos + 1);
	}
}

std::string Game::get_current_player_name()
{
	return current_player->get_player_name();
}

void Game::endturn()
{
	increment_current_player();
}

void Game::move_player(int true_numbers_of_questions)
{
	current_player->set_position(true_numbers_of_questions);
}

void Game::add_questions()
{
	QuestionsList *ql = new QuestionsList();
	questions_list = ql->get_questions_list();
}

QList<Question *> Game::get_questions_list()
{
	return questions_list;
}

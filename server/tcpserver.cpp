#include "tcpserver.h"

#include "constants.h"

#include <QDataStream>
#include <QTcpServer>
#include <QtNetwork>
#include <cstdlib>
#include <ctime>

TcpServer::TcpServer(QObject *parent) : QObject(parent)
{
	srand(time(NULL));
}

TcpServer::~TcpServer()
{
}

void TcpServer::startServer()
{
	m_clients = QMap<QTcpSocket *, int>();
	start_timer = new QTimer();

	ql = new QuestionsList();
	ql->get_questions_list();

	m_server = new QTcpServer();
	m_server->setMaxPendingConnections(10);
	connect(m_server, SIGNAL(newConnection()), this, SLOT(newClientConnection()));

	if (m_server->listen(QHostAddress::Any, 8001)) {
		qDebug() << "Server has started. Listening to port 8001.";
	} else {
		qDebug() << "Server failed to start.Error: " + m_server->errorString();
	}
}

void TcpServer::newClientConnection()
{

	QTcpSocket *client = m_server->nextPendingConnection();
	QString ipAddress = client->peerAddress().toString();
	int port = client->peerPort();

	if (m_clients.size() >= 4) {
		QByteArray block;
		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::TOO_MANY_PLAYERS;
		client->write(block);
		return;
	}

	connect(client, &QTcpSocket::disconnected, this, &TcpServer::socketDisconnected);
	connect(client, &QTcpSocket::readyRead, this, &TcpServer::socketReadyRead);
	connect(client, &QTcpSocket::stateChanged, this, &TcpServer::socketStateChanged);

	m_names.push_back("");
	m_clients.insert(client, m_clients.size());

	qDebug() << "Socket connected from " + ipAddress + ":" + QString::number(port);

	if (!should_start) {
		should_start = true;
		prepare_start();
	}
}

void TcpServer::socketDisconnected()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(QObject::sender());
	QString socketIpAdress = client->peerAddress().toString();
	int port = client->peerPort();
	qDebug() << "Socket disconnected from " + socketIpAdress + ":" + QString::number(port);
	m_clients.remove(client);
	if (m_clients.size() == 0) {
		m_server->close();
		qDebug() << "stopping server";
		exit(EXIT_SUCCESS);
		return;
	}
	QByteArray block;
	QDataStream out(&block, QIODevice::WriteOnly);
	out.setVersion(QDataStream::Qt_5_9);
	out << Constants::CLIENT_DISCONNECTED;

	for (auto cl : m_clients.keys()) {
		if (cl->isOpen())
			cl->write(block);
	}
}

void TcpServer::socketReadyRead()
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(QObject::sender());
	QString socketIpAdress = client->peerAddress().toString();
	// int port =client->peerPort();

	in.setDevice(client);
	in.setVersion(QDataStream::Qt_5_9);
	in.startTransaction();

	int type;
	in >> type;

	bool hasMessage = false;
	QByteArray block;

	if (type == Constants::CLIENT_SEND_NAME) {
		QByteArray message;
		in >> message;
		m_names[m_clients.find(client).value()] = QString::fromUtf8(message);
	} else if (type == Constants::MOVE_PLAYER) {
		qDebug() << "move";
		int spaces;
		in >> spaces;

		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::MOVE_PLAYER << spaces;
		hasMessage = true;
	} else if (type == Constants::OPEN_CARD) {
		qDebug() << "open card ";
		int card_type;
		in >> card_type;

		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::OPEN_CARD << card_type;

		if (card_type == Constants::CARD_TRAVEL || card_type == Constants::CARD_MONEY ||
			card_type == Constants::CARD_SWEETS || card_type == Constants::CARD_IMMUNITY) {
			int amount;
			in >> amount;
			out << amount;
		}

		hasMessage = true;
	} else if (type == Constants::END_TURN) {
		qDebug() << "end turn";

		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::END_TURN;
		hasMessage = true;

		QTimer::singleShot(100, this, &TcpServer::start_turn);
	} else {
		qDebug() << "else ";
	}

	if (!in.commitTransaction())
		return;

	if (hasMessage)
		sendMessageToClients(block);
}

void TcpServer::socketStateChanged(QAbstractSocket::SocketState state)
{
	QTcpSocket *client = qobject_cast<QTcpSocket *>(QObject::sender());
	QString socketIpAdress = client->peerAddress().toString();
	int port = client->peerPort();

	QString data = QString(client->readAll());

	QString desc;

	if (state == QAbstractSocket::UnconnectedState)
		desc = "The socket is not connected.";
	else if (state == QAbstractSocket::HostLookupState)
		desc = "The socket is performing a host name lookup.";
	else if (state == QAbstractSocket::ConnectingState)
		desc = "The socket has started establishing a connection.";
	else if (state == QAbstractSocket::ConnectedState)
		desc = "A connection is established.";
	else if (state == QAbstractSocket::BoundState)
		desc = "The socket is bound to an address and port.";
	else if (state == QAbstractSocket::ClosingState)
		desc = "The socket is about to close.";
	else if (state == QAbstractSocket::ListeningState)
		desc = "For internal use only.";

	qDebug() << "Socket state changed (" + socketIpAdress + ":" + QString::number(port) + ")" + desc;
}

void TcpServer::sendMessageToClients(QByteArray message)
{
	if (m_clients.size() > 0) {
		for (auto client : m_clients.keys()) {
			int i = m_clients[client];
			if (i != turn) {
				if (client->isOpen() && client->isWritable()) {
					qDebug() << "sent move " << i;
					client->write(message);
				}
			}
		}
	}
}

void TcpServer::start_game()
{
	game_started = true;
	if (m_clients.size() == 1) {

		QByteArray block;
		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::NOT_ENOUGH_PLAYERS;

		for (auto cl : m_clients.keys()) {
			cl->write(block);
		}

		prepare_start();
	} else {
		for (auto cl : m_clients.keys()) {

			QByteArray block;
			QDataStream out(&block, QIODevice::WriteOnly);
			out.setVersion(QDataStream::Qt_5_9);

			out << Constants::SERVER_START_GAME;
			out << (int)m_names.size();
			for (auto name : m_names) {
				out << name.toUtf8();
			}

			cl->write(block);
		}

		QTimer::singleShot(100, this, &TcpServer::start_turn);
	}
}

void TcpServer::start_turn()
{
	turn++;
	turn = turn % m_clients.size();
	for (auto cl : m_clients.keys()) {
		int i = m_clients[cl];
		if (i == turn) {
			qDebug() << "i==turn" << i;
			QByteArray block;
			QDataStream out(&block, QIODevice::WriteOnly);
			out.setVersion(QDataStream::Qt_5_9);

			int roll = rand() % 6 + 1;

			out << Constants::QUESTIONS << roll;
			for (int i = 0; i < roll; i++) {
				out << QString::fromStdString(ql->get_questions_list()[last_question]->get_question()).toUtf8();
				out << QString::fromStdString(ql->get_questions_list()[last_question]->get_answer()).toUtf8();
				last_question++;
			}

			cl->write(block);
		} else {
			qDebug() << "i!=turn" << i;
			QByteArray block;
			QDataStream out(&block, QIODevice::WriteOnly);
			out.setVersion(QDataStream::Qt_5_9);
			out << Constants::WAIT_TURN;

			cl->write(block);
		}
	}
}
void TcpServer::prepare_start()
{
	current_time = timer_length;
	start_timer->setInterval(1000);
	connect(start_timer, SIGNAL(timeout()), this, SLOT(on_timer()));
	start_timer->start();
}

void TcpServer::on_timer()
{
	current_time--;
	if (current_time == 0) {
		start_timer->stop();
		start_game();
	} else {
		QByteArray block;
		QDataStream out(&block, QIODevice::WriteOnly);
		out.setVersion(QDataStream::Qt_5_9);
		out << Constants::TIME_UNTIL_START << current_time;

		for (auto cl : m_clients.keys()) {
			cl->write(block);
		}
	}
}

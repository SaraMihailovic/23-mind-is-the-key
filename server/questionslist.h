#ifndef QUESTIONSLIST_H
#define QUESTIONSLIST_H

#include "question.h"

#include <QList>
class QuestionsList
{
public:
	QuestionsList();
	QuestionsList(QList<Question *> list) : questions_list(list)
	{
	}
	~QuestionsList();

	void add_questions();
	QList<Question *> get_questions_list();

private:
	QList<Question *> questions_list;
};

#endif // QUESTIONSLIST_H

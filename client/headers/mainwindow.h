#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "headers/game.h"
#include "headers/login.h"
#include "headers/player.h"
#include "headers/question.h"
#include "headers/questionsbox.h"
#include "ui_mainwindow.h"

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMainWindow>
#include <QtCore>
#include <QtGui>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

	void update_display(); // update graphics on board
	void set_space_list(); // generate space list from elements
	QList<QLabel *> get_space_list()
	{
		return this->space_list;
	}

	void set_player_list();
	QList<Player *> get_player_list()
	{
		return player_list;
	}

	void display_options();
	void set_game(Game *game)
	{
		this->game = game;
	}
	Game *get_game()
	{
		return this->game;
	}
	void start_game(QVector<QString> player_name);
	void start_game(QString p1, QString p2, QString p3, QString p4, int player);

	void set_true_answer(int true_answ)
	{
		true_answers = true_answ;
	}
	int get_true_answers()
	{
		return true_answers;
	}

	void delete_die_questions();

private slots:
	void on_roll_button_clicked();
	void on_end_roll_button_clicked();
	void on_question_button_clicked();
	void on_card_button_clicked();
	void on_action_how_to_play_triggered();
	void on_action_quit_triggered();
	void on_action_help_triggered();

private:
	int ind = 0;
	QVector<QString> player_name;
	Ui::MainWindow *ui;
	Game *game;
	int die;
	int true_answers;

	QList<Question *> questions_list;
	QList<QLabel *> space_list;	 // list of label elements in the ui
	QList<Player *> player_list; // list of player
};

#endif // MAINWINDOW_H

#ifndef LUCKYCARD_H
#define LUCKYCARD_H
#include "headers/card.h"

#include <QString>
#include <iostream>
#include <string>
#include <vector>

enum class LuckyType { Travelling, Sweets, Money, MoveForwards, Immunity };

class LuckyCard : public Card
{

public:
	LuckyCard();
	LuckyCard(const std::string &name, const std::string &text, const std::vector<LuckyType> &types);

	~LuckyCard();

	void set_card_text(std::string text) override;
	std::string get_card_text() override;
	std::string Type() const;
	std::string show() const override;

private:
	std::vector<LuckyType> card_types;
	std::string card_text;
};
#endif // LUCKYCARD_H

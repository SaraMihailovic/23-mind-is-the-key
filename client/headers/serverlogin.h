#ifndef SERVERLOGIN_H
#define SERVERLOGIN_H

#include <QDataStream>
#include <QDebug>
#include <QDialog>
#include <QObject>
#include <QString>
#include <QTcpSocket>

namespace Ui
{
class ServerLogin;
}

class ServerLogin : public QDialog
{
	Q_OBJECT

public:
	explicit ServerLogin(QWidget *parent = nullptr);
	~ServerLogin();

private slots:
	void on_startGame_clicked();

public slots:
	void socketReadyRead();
	void socketConnected();
	void socketDisconnected();

private:
	Ui::ServerLogin *ui;
	bool connectedToHost;
	QTcpSocket *socket;
	QDataStream in;
	void set_background_image(QString image_path);
};

#endif // SERVERLOGIN_H

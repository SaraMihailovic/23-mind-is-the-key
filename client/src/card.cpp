#include "headers/card.h"

#include <QString>

Card::Card()
{
	m_card_name = "Card name";
}

Card::Card(const std::string &name) : m_card_name(name)
{
}

Card ::~Card()
{
	std::cout << "This is destructor for Card!" << std::endl;
}

void Card::set_name(std::string name)
{
	m_card_name = name;
}

std::string Card::get_name()
{
	return m_card_name;
}

std::string Card::show() const
{
	return m_card_name;
}

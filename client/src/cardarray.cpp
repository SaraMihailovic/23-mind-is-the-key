#include <headers/cardarray.h>
#include <headers/player.h>

CardArray::CardArray()
{

	v2 = {LuckyType::Travelling, LuckyType::Sweets, LuckyType::Money, LuckyType::MoveForwards,
		  LuckyType::Immunity};
	v = {BadType::LoseMoney, BadType::LoseAll, BadType::HalfMoney, BadType::MoveBackwards};
}

CardArray::~CardArray()
{
}

void CardArray::create_cards(Player *currentPlayer, Ui::MainWindow *ui)
{
	int random = rand() % 2 + 1;
	if (random == 1) {

		LuckyCard *luck1t = new LuckyCard("n1", "You got a travel voucher to Paris!", v2);

		LuckyCard *luck2t = new LuckyCard("n1", "You got a travel voucher to Rome!", v2);

		LuckyCard *luck3t = new LuckyCard("n1", "You got a travel voucher to Cuba!", v2);

		LuckyCard *luck4t = new LuckyCard("n1", "You got a travel voucher to London!", v2);

		LuckyCard *luck5t = new LuckyCard("n1", "You got a travel voucher to Dubai!", v2);

		LuckyCard *luck1s = new LuckyCard("n2", "You got sweets", v2);

		LuckyCard *luck1m = new LuckyCard("n3", "You got money!", v2);

		LuckyCard *luck4 = new LuckyCard("n4", "Move forwards one step!", v2);

		LuckyCard *luck5 = new LuckyCard("n5", "You got immunity until the end of game!", v2);

		int value1 = rand() % 5 + 1;
		int c = rand() % 5;

		switch (v2[value1]) {
		case LuckyType::Travelling:
			switch (c) {
			case 0:
				ui->label_card->setText(QString::fromStdString(luck1t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:/images/images/paris.png);");
				break;
			case 1:
				ui->label_card->setText(QString::fromStdString(luck2t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:/images/images/rome.png);");
				break;
			case 2:
				ui->label_card->setText(QString::fromStdString(luck3t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:/images/images/cuba.png);");
				break;
			case 3:
				ui->label_card->setText(QString::fromStdString(luck4t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:/images/images/london.png);");
				break;
			case 4:
				ui->label_card->setText(QString::fromStdString(luck5t->get_card_text()));
				ui->card_button->setStyleSheet("border-image:url(:/images/images/dubai.png);");
				break;
			}

			currentPlayer->set_travell(1);
			if (currentPlayer->get_number() == 1) {
				ui->travel1->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 2) {
				ui->travel2->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 3) {
				ui->travel3->setText(QString::number(currentPlayer->get_travell()));
			} else if (currentPlayer->get_number() == 4) {
				ui->travel4->setText(QString::number(currentPlayer->get_travell()));
			}
			break;
		case LuckyType::Sweets:
			ui->label_card->setText(QString::fromStdString(luck1s->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/Sweets.png);");
			currentPlayer->set_sweets(1);
			if (currentPlayer->get_number() == 1) {
				ui->sweet1->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 2) {
				ui->sweet2->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 3) {
				ui->sweet3->setText(QString::number(currentPlayer->get_sweets()));
			} else if (currentPlayer->get_number() == 4) {
				ui->sweet4->setText(QString::number(currentPlayer->get_sweets()));
			}
			break;
		case LuckyType::Money:
			ui->label_card->setText(QString::fromStdString(luck1m->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/Money.png);");
			if (currentPlayer->get_position() < 6) {
				currentPlayer->set_money_amount(100);
			} else if (currentPlayer->get_position() < 11) {
				currentPlayer->set_money_amount(500);
			} else if (currentPlayer->get_money_amount() < 17) {
				currentPlayer->set_money_amount(1000);
			} else if (currentPlayer->get_money_amount() < 25) {
				currentPlayer->set_money_amount(2000);
			}
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case LuckyType::MoveForwards:
			ui->label_card->setText(QString::fromStdString(luck4->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/MoveForwards.png);");
			currentPlayer->move_one_step();
			break;
		case LuckyType::Immunity:
			ui->label_card->setText(QString::fromStdString(luck5->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/Immunity.png);");
			currentPlayer->set_ind_immunity(1);
			if (currentPlayer->get_number() == 1) {
				ui->immunity1->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 2) {
				ui->immunity2->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 3) {
				ui->immunity3->setText(QString::number(currentPlayer->get_ind_immunity()));
			} else if (currentPlayer->get_number() == 4) {
				ui->immunity4->setText(QString::number(currentPlayer->get_ind_immunity()));
			}
			break;
		}

	} else {
		int value2 = rand() % 4 + 1;

		BadCard *bad1 = new BadCard("n1", "You lose all money!", v);

		BadCard *bad2 = new BadCard("n2", "You lose everything", v);

		BadCard *bad3 = new BadCard("n3", "You lose half of your money!", v);

		BadCard *bad4 = new BadCard("n4", "Move backwards one step!", v);

		switch (v[value2]) {
		case BadType::LoseMoney:
			ui->label_card->setText(QString::fromStdString(bad1->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/LoseMoney.png);");
			currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case BadType::LoseAll:
			ui->label_card->setText(QString::fromStdString(bad2->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/LoseAll.png);");
			if (currentPlayer->get_ind_immunity() == 0) {
				currentPlayer->set_money_amount(-currentPlayer->get_money_amount());
				currentPlayer->set_travell(-currentPlayer->get_travell());
				currentPlayer->set_sweets(-currentPlayer->get_sweets());
				if (currentPlayer->get_number() == 1) {
					ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel1->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet1->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 2) {
					ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel2->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet2->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 3) {
					ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel3->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet3->setText(QString::number(currentPlayer->get_sweets()));
				} else if (currentPlayer->get_number() == 4) {
					ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
					ui->travel4->setText(QString::number(currentPlayer->get_travell()));
					ui->sweet4->setText(QString::number(currentPlayer->get_sweets()));
				}
			}
			break;
		case BadType::HalfMoney:
			ui->label_card->setText(QString::fromStdString(bad3->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/HalfMoney.png);");
			currentPlayer->set_money_amount(-(currentPlayer->get_money_amount()) / 2);
			if (currentPlayer->get_number() == 1) {
				ui->money1->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 2) {
				ui->money2->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 3) {
				ui->money3->setText(QString::number(currentPlayer->get_money_amount()));
			} else if (currentPlayer->get_number() == 4) {
				ui->money4->setText(QString::number(currentPlayer->get_money_amount()));
			}
			break;
		case BadType::MoveBackwards:
			ui->label_card->setText(QString::fromStdString(bad4->get_card_text()));
			ui->card_button->setStyleSheet("border-image:url(:/images/images/MoveBackwards.png);");
			if (currentPlayer->get_position() != 0) {
				currentPlayer->move_one_step_back();
			}
			break;
		}
	}
}

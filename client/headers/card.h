#ifndef CARD_H
#define CARD_H

#include <QString>
#include <iostream>
#include <string>
#include <vector>

class Card
{

public:
	Card();
	Card(const std::string &name);

	virtual ~Card();

	void set_name(std::string name);

	std::string get_name();

	virtual void set_card_text(std::string text) = 0;
	virtual std::string get_card_text() = 0;
	virtual std::string show() const;

private:
	std::string m_card_name;
};

#endif

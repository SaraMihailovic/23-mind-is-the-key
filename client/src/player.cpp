#include "headers/player.h"

Player::Player()
{

	m_player_name = "Player1";
	m_piece = 1;
	m_number = 0;
	m_money_amount = 0;
	m_position = 0;
}

Player::Player(std::string player_name, std::string piece, int number, int money_amount)
{
	m_player_name = player_name;
	m_money_amount = money_amount;
	m_number = number;
	set_piece(piece);
	m_position = 0;
}

Player::~Player()
{
}

std::string Player::get_player_name()
{
	return m_player_name;
}

int Player::get_piece()
{
	return m_piece;
}

int Player::get_number()
{
	return m_number;
}

int Player::get_money_amount()
{
	return m_money_amount;
}

int Player::get_ind_immunity()
{
	return ind_immunity;
}

int Player::get_travell()
{
	return travell;
}

int Player::get_sweets()
{
	return sweets;
}

unsigned int Player::get_position()
{
	return m_position;
}

void Player::set_player_name(std::string name)
{

	m_player_name = name;
}

void Player::set_number(int num)
{
	m_number = num;
}

void Player::set_money_amount(int money)
{
	m_money_amount = m_money_amount + money;
}

void Player::set_travell(int num)
{
	travell += num;
}

void Player::set_sweets(int num)
{
	sweets += num;
}

void Player::set_ind_immunity(int immunity)
{
	ind_immunity = immunity;
}

void Player::set_piece(std::string piece)
{
	if (piece == "PhilipMartin") {
		m_piece = 1;
	} else if (piece == "Columbus") {
		m_piece = 2;
	} else if (piece == "Explorer") {
		m_piece = 3;
	} else {
		m_piece = 4;
	}
}
void Player::set_position(int number)
{
	m_position += number;
	if (m_position > 21) {
		m_position = m_position % 21;
	}
}

void Player::move_one_step()
{
	set_position(1);
}

void Player::move_one_step_back()
{
	set_position(-1);
}

#ifndef GAMEOVER_H
#define GAMEOVER_H

#include "player.h"

#include <QMainWindow>

namespace Ui
{
class Gameover;
}

class Gameover : public QMainWindow
{
	Q_OBJECT

public:
	explicit Gameover(QList<Player *> list, QWidget *parent = nullptr);
	~Gameover();
	void set_background_image(QString image_path);

private:
	Ui::Gameover *ui;
	QList<Player *> player_list;
};

#endif // GAMEOVER_H

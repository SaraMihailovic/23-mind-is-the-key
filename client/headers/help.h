#ifndef HELP_H
#define HELP_H

#include <QDialog>

namespace Ui
{
class Help;
}

class Help : public QDialog
{
	Q_OBJECT

public:
	explicit Help(QWidget *parent = nullptr);
	~Help();

private:
	Ui::Help *ui;
	void set_background_image(QString image11_path);
};

#endif // HELP_H

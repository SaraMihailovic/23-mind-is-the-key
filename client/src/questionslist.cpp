#include "headers/questionslist.h"

QuestionsList::QuestionsList()
{
	Question *q1 = new Question("Which season is in April?", "Spring");
	questions_list.push_back(q1);

	Question *q2 = new Question("What is the highest mountain in Britain?", "Ben Nevis");
	questions_list.push_back(q2);

	Question *q3 = new Question("What is the smallest country in the world?", "Vatican");
	questions_list.push_back(q3);

	Question *q4 = new Question("Which English city was once known as Duroliponte?", "Cambridge");
	questions_list.push_back(q4);

	Question *q5 = new Question("What colour is found on 75% of the world’s flags?", "Red");
	questions_list.push_back(q5);

	Question *q6 = new Question("What is the next number in the sequence: 2, 7, 22, 67, 202, …?", "607");
	questions_list.push_back(q6);

	Question *q7 = new Question("What’s the national animal of Australia?", "Kangaroo");
	questions_list.push_back(q7);

	Question *q8 = new Question("How many faces has an icosahedron?", "20");
	questions_list.push_back(q8);

	Question *q9 = new Question("What is the capital city of Iceland?", "Reykjavik");
	questions_list.push_back(q9);

	Question *q10 = new Question("What is the name of the longest river in the world?", "Nile");
	questions_list.push_back(q10);

	Question *q11 = new Question("How many players are there in a rugby league team?", "13");
	questions_list.push_back(q11);

	Question *q12 = new Question("Who composed the Moonlight Sonata?", "Beethoven");
	questions_list.push_back(q12);

	Question *q13 = new Question("How many time zones are there in Russia?", "11");
	questions_list.push_back(q13);

	Question *q14 = new Question("What’s the national flower of Japan?", "Cherry blossom");
	questions_list.push_back(q14);

	Question *q15 = new Question("What is the middle name of Angela Merkel?", "Dorota");
	questions_list.push_back(q15);

	Question *q16 = new Question("What is the most famous Mexican beer?", "Corona");
	questions_list.push_back(q16);

	Question *q17 = new Question("Who invented the iconic Little Black Dress?", "Coco Chanel");
	questions_list.push_back(q17);

	Question *q18 = new Question("What city do The Beatles come from?", "Liverpool");
	questions_list.push_back(q18);

	Question *q19 = new Question("There are McDonald’s on every continent except one.(True/False)", "True");
	questions_list.push_back(q19);

	Question *q20 = new Question("A woman has walked on the Moon.(True/False)", "False");
	questions_list.push_back(q20);

	Question *q21 = new Question("Vietnamese is an official language in Canada.(True/False)", "False");
	questions_list.push_back(q21);

	Question *q22 = new Question("Which planet is the closest to the Sun?", "Mercury");
	questions_list.push_back(q22);

	Question *q23 = new Question("How many oceans are there in the world?", "5");
	questions_list.push_back(q23);

	Question *q24 = new Question("Who painted the Water Lilies?", "Monet");
	questions_list.push_back(q24);

	Question *q25 = new Question("Which football team is known as ‘The Red Devils’?", "Manchester United");
	questions_list.push_back(q25);

	Question *q26 = new Question("How many decimeters is 0.2?", "2000");
	questions_list.push_back(q26);

	Question *q27 =
		new Question("Which artist painted the ceiling of the Sistine Chapel in Rome?", "Michelangelo");
	questions_list.push_back(q27);

	Question *q28 = new Question("How many keys does a classic piano have?", "88");
	questions_list.push_back(q28);

	Question *q29 = new Question("Does Novak Djokovic have a gold medal from the Olympic Games?", "No");
	questions_list.push_back(q29);

	Question *q30 = new Question("What is the largest country in the world?", " Russia");
	questions_list.push_back(q30);

	Question *q31 = new Question("Where would you find Mount Kilimanjaro?", "Africa");
	questions_list.push_back(q31);

	Question *q32 = new Question("What's the capital of Russia?", "Moscow");
	questions_list.push_back(q32);

	Question *q33 = new Question("How many colours are in the rainbow?", "7");
	questions_list.push_back(q33);

	Question *q34 = new Question("What does kg stand for?", "Kilograms");
	questions_list.push_back(q34);

	Question *q35 = new Question("What river runs through Paris?", "The Seine");
	questions_list.push_back(q35);

	Question *q36 = new Question("What university did Stephen Hawking go to?", "Cambridge");
	questions_list.push_back(q36);

	Question *q37 = new Question("What continent is Nigeria part of?", "Africa");
	questions_list.push_back(q37);

	Question *q38 = new Question("How many valves does the heart have?", "4");
	questions_list.push_back(q38);

	Question *q39 = new Question("If you have cryophobia, what are you afraid of?", "cold");
	questions_list.push_back(q39);

	Question *q40 = new Question("Who won the Best Actress Award at the most recent Oscars?", "Renée Zellweger");
	questions_list.push_back(q40);

	Question *q41 = new Question("What’s the chemical symbol for gold? ", "Au");
	questions_list.push_back(q41);

	Question *q42 = new Question("How many permanent teeth does a dog have?", "42");
	questions_list.push_back(q42);

	Question *q43 =
		new Question("Which country in the world is believed to have the most miles of motorway?", "China");
	questions_list.push_back(q43);

	Question *q44 = new Question("In what decade was pop icon Madonna born?", "1950s");
	questions_list.push_back(q44);

	Question *q45 = new Question("Which European city hosted the 1936 Summer Olympics?", "Berlin");
	questions_list.push_back(q45);

	Question *q46 = new Question("What percentage of people in the world have green eyes?", "2%");
	questions_list.push_back(q46);

	Question *q47 = new Question("What is the largest fish in the ocean?", "whale shark");
	questions_list.push_back(q47);

	Question *q48 = new Question("What is the most common blood type in the world?", "0+");
	questions_list.push_back(q48);

	Question *q49 = new Question("What is the name of Superman's planet?", "Krypton");
	questions_list.push_back(q49);

	Question *q50 = new Question("Which country won the first ever soccer World Cup in 1930?", "Urugay");
	questions_list.push_back(q50);

	Question *q51 = new Question("How many bones does the human body have in total?", "206");
	questions_list.push_back(q51);

	Question *q52 = new Question("What percentage of people in the world are left-handed?", "10%");
	questions_list.push_back(q52);

	Question *q53 = new Question("If f(x) is an odd function, then | f(x) | is?", "even function");
	questions_list.push_back(q53);

	Question *q54 = new Question("What is the most populous country in the world?", "China");
	questions_list.push_back(q54);

	Question *q55 = new Question("Which empire was ruled by the Severan dynasty?", "Roman");
	questions_list.push_back(q55);

	Question *q56 = new Question(
		"The first commercially available language was FORTRAN (FORmula TRANslation), developed in "
		"1956.(True/False)",
		"True");
	questions_list.push_back(q56);

	Question *q57 = new Question("In what part of the body would you find the fibula?", "leg");
	questions_list.push_back(q57);

	Question *q58 = new Question("What language did Anne Frank write her diary in?", "Dutch");
	questions_list.push_back(q58);

	Question *q59 = new Question("What is the animal on the Papua New Guinea flag?", "bird of paradise");
	questions_list.push_back(q59);

	Question *q60 = new Question("What is the pen name of Charles Lutwidge Dodgson?", "Lewis Carroll");
	questions_list.push_back(q60);

	Question *q61 =
		new Question("Which planet like Uranus spins in the opposite direction to the others?", "Venus");
	questions_list.push_back(q61);

	Question *q62 = new Question("Which is the longest river in Europe?", "Volga");
	questions_list.push_back(q62);

	Question *q63 = new Question("When started the first World War?", "1914");
	questions_list.push_back(q63);

	Question *q64 = new Question("Which vitamin is the only one that you will not find in an egg?", "C");
	questions_list.push_back(q64);

	Question *q65 = new Question(
		"In tennis, what piece of fruit is found at the top of the men's Wimbledon trophy?", "pineapple");
	questions_list.push_back(q65);

	Question *q66 = new Question("What is seven cubed?", "343");
	questions_list.push_back(q66);

	Question *q67 = new Question("Which operating system does a Google Pixel phone use?", "Android");
	questions_list.push_back(q67);

	Question *q68 = new Question("What’s the chemical symbol for silver?", "Ag");
	questions_list.push_back(q68);

	Question *q69 = new Question("In which century did Rembrandt live?", "17");
	questions_list.push_back(q69);

	Question *q70 = new Question("What is part of a database that holds only one type of information?", "Field");
	questions_list.push_back(q70);

	Question *q71 = new Question(
		"Which singer was known amongst other things as ‘The King of Pop’ and ‘The Gloved One’?",
		"Michael Jackson");
	questions_list.push_back(q71);

	Question *q72 = new Question("What is the name of the biggest technology company in South Korea?", "Samsung");
	questions_list.push_back(q72);

	Question *q73 = new Question("What kind of food is Penne?", "Pasta");
	questions_list.push_back(q73);

	Question *q74 = new Question("How many Pirates of the Caribbean films have been released?", "5");
	questions_list.push_back(q74);

	Question *q75 = new Question("In which year was the Microsoft XP operating system released?", "2001");
	questions_list.push_back(q75);

	Question *q76 = new Question("Where was Frida Kahlo born?", "Mexico");
	questions_list.push_back(q76);

	Question *q77 = new Question("What is the currency of Portugal?", "Euro");
	questions_list.push_back(q77);

	Question *q78 = new Question("How many weeks are there in a year?", "52");
	questions_list.push_back(q78);

	Question *q79 = new Question("You can sneeze in your sleep.(True/False)", "False");
	questions_list.push_back(q79);

	Question *q80 = new Question(
		"What Italian word for 'Scratched Drawing' can be found on walls all over the world?", "Graffitti");
	questions_list.push_back(q80);

	Question *q81 = new Question("Where the Mona Lisa is exposed?", "Louvre");
	questions_list.push_back(q81);

	Question *q82 = new Question("Which movie won the  Oscar 2020?", "Parasite");
	questions_list.push_back(q82);

	Question *q83 = new Question("Who wrote 'Lamet nad Beogradom'?", "Milos Crnjanski");
	questions_list.push_back(q83);

	Question *q84 = new Question("Who discovered America?", "Cristopher Columbus");
	questions_list.push_back(q84);

	Question *q85 = new Question("Who wrote 'The Iliad' and 'The Odyssey'?", "Homer");
	questions_list.push_back(q85);

	Question *q86 = new Question("What is the symbol for oxygen?", "O");
	questions_list.push_back(q86);

	Question *q87 = new Question("Which email service is owned by Microsoft?", "Hotmail");
	questions_list.push_back(q87);

	Question *q88 = new Question("What is the chemical symbol for potassium?", "K");
	questions_list.push_back(q88);

	Question *q89 = new Question("Which country produce the most coffe in the world?", "Brazil");
	questions_list.push_back(q89);

	Question *q90 = new Question("Which company has Bugatti,Lamborghini,Audi,Porche models? ", "Volkswagen");
	questions_list.push_back(q90);

	Question *q91 = new Question("From which city under the sea is Aquaman?", "Atlantist");
	questions_list.push_back(q91);

	Question *q92 = new Question("Which continent is the largest?", "Asia");
	questions_list.push_back(q92);

	Question *q93 = new Question("On which day and month is The Chinese New Year celebrated?", "25th January");
	questions_list.push_back(q93);

	Question *q94 = new Question("Which country  did AC/DC orginate in?", "Australia");
	questions_list.push_back(q94);

	Question *q95 = new Question("In which year World War II began?", "1939?");
	questions_list.push_back(q95);
}

QuestionsList::~QuestionsList()
{
}

QList<Question *> QuestionsList::get_questions_list()
{
	return questions_list;
}

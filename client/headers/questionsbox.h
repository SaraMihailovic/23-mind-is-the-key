#ifndef QUESTIONSBOX_H
#define QUESTIONSBOX_H

#include "headers/question.h"

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QTextBrowser>
#include <QVector>

namespace Ui
{
class QuestionsBox;
}

class QuestionsBox : public QDialog
{
	Q_OBJECT

public:
	explicit QuestionsBox(QList<Question *> list, QWidget *parent = nullptr);
	~QuestionsBox();

	int get_die();
	int get_true_answers();

private slots:
	void show_time();
	void on_ok_button_clicked();

private:
	Ui::QuestionsBox *ui;
	QVector<QTextBrowser *> question_boxes;
	QVector<QLineEdit *> answer_boxes;
	QVector<QLabel *> labels;
	void set_background_image(QString image_path);
	int true_answers = 0;
	QList<Question *> questions_list;
	int n_questions = 0;
	int time = 30;
	QTimer *timer2;
};

#endif // QUESTIONSBOX_H

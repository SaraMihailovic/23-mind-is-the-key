#include "tcpserver.h"

#include <QCoreApplication>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	TcpServer *myserver = new TcpServer();
	myserver->startServer();

	return a.exec();
}

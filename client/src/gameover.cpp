#include "headers/gameover.h"

#include "headers/mainwindow.h"
#include "headers/player.h"
#include "ui_gameover.h"

#include <QWidget>
#include <iostream>

bool less_than(Player *p1, Player *p2)
{
	return p1->get_position() >= p2->get_position();
}
Gameover::Gameover(QList<Player *> list, QWidget *parent)
	: QMainWindow(parent), ui(new Ui::Gameover), player_list(list)
{
	ui->setupUi(this);

	set_background_image(":images/images/old.png");
	ui->gold->setStyleSheet("border-image:url(:images/images/gold.png);");
	ui->silver->setStyleSheet("border-image:url(:images/images/silver.png);");

	// Sort player positions
	std::sort(player_list.begin(), player_list.end(), less_than);

	if (player_list.length() == 2) {
		ui->name1->setText(QString::fromStdString(player_list[0]->get_player_name()));
		ui->name2->setText(QString::fromStdString(player_list[1]->get_player_name()));
		if (player_list[0]->get_piece() == 1) {
			ui->lbl_first->setStyleSheet("border-image:url(:images/images/PhilipMartin.png);");
			ui->lbl_second->setStyleSheet("border-image:url(:images/images/Columbus.png);");
		} else {
			ui->lbl_second->setStyleSheet("border-image:url(:images/images/PhilipMartin.png);");
			ui->lbl_first->setStyleSheet("border-image:url(:images/images/Columbus.png);");
		}
	} else if (player_list.length() == 3) {
		ui->third->setText(QString("Third"));
		ui->bronze->setStyleSheet("border-image:url(:images/images/bronze.png);");
		ui->name1->setText(QString::fromStdString(player_list[0]->get_player_name()));
		ui->name2->setText(QString::fromStdString(player_list[1]->get_player_name()));
		ui->name3->setText(QString::fromStdString(player_list[2]->get_player_name()));

		ui->lbl_first->setStyleSheet("border-image:url(:images/images/PhilipMartin.png);");
		ui->lbl_second->setStyleSheet("border-image:url(:images/images/Columbus.png);");
		ui->lbl_third->setStyleSheet("border-image:url(:images/images/Explorer.png);");
	} else {
		ui->third->setText(QString("Third"));
		ui->fourth->setText(QString("Fourth"));
		ui->bronze->setStyleSheet("border-image:url(:images/images/bronze.png);");
		ui->name1->setText(QString::fromStdString(player_list[0]->get_player_name()));
		ui->name2->setText(QString::fromStdString(player_list[1]->get_player_name()));
		ui->name3->setText(QString::fromStdString(player_list[2]->get_player_name()));
		ui->name4->setText(QString::fromStdString(player_list[3]->get_player_name()));
		ui->lbl_first->setStyleSheet("border-image:url(:images/images/PhilipMartin.png);");
		ui->lbl_second->setStyleSheet("border-image:url(:images/images/Columbus.png);");
		ui->lbl_third->setStyleSheet("border-image:url(:images/images/Explorer.png);");
		ui->lbl_fourth->setStyleSheet("border-image:url(:images/images/Magellan.png);");
	}
}

Gameover::~Gameover()
{
	delete ui;
}

void Gameover::set_background_image(QString image_path)
{
	QPixmap bkgnd(image_path);
	bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Background, bkgnd);
	this->setPalette(palette);
}
